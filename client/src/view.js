import { baseURL } from "./config.js";

export const show = (data) => {
  const item = document.getElementById("products-list");
  let data_item = "";
  item.innerHTML = "";
  data_item += '<div class="row">';
  for (let i = 0; i <= data.length - 1; i++) {
    data_item += '<div class="col-md-4 card">';

    let imageUrl;
    if (typeof data[i].image !== "undefined")
      imageUrl = baseURL + "/uploads/images/products/" + data[i].image;
    else imageUrl = "";

    data_item +=
      '<div class="p-2"><h2>' +
      data[i].name +
      "</h2>" +
      '</div><img width="200" height="200" src="' +
      imageUrl +
      '"><hr> <h2>Description</h2>' +
      data[i].description;

    for (let j = 0; j <= data[i].offers.length - 1; j++) {
      data_item += "<ul>";

      data_item += "<li><b> Offer " + (j + 1) + "</b></li>";

      data_item += '<li><a href="' + data[i].offers[j].url + '">url</a></li>';
      data_item +=
        "<li>" +
        data[i].offers[j].price +
        " " +
        data[i].offers[j].priceCurrency +
        "</li>";

      data_item += "</ul>";
    }

    data_item += '<hr class="bg-info">';
    data_item += "</div>";
  }
  data_item += "</div>";

  item.innerHTML = data_item;
};
