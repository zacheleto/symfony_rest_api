# Products and Offers API
This projects has a lot of core Rest API concepts to it

### standards and tech used

* Schema.org and Google data-types markup for Entity properties
* Symfony api_platform 
* Sort api query result using API platform order, filter
* use Search filter of API Platform
* API Platform serialization groups (normalization and denormalizaion)
* Testable stuctured json-LD data with Goole tools such as (https://search.google.com/structured-data/testing-tool/u/0/ and https://search.google.com/test/rich-results)
* Built-in Event Listeners (kernetl.view | hooks)
* API subResource feature for nested API endpoints
* functional tests


### Features

* admin can login
* admin can logout
* admin can add products
* admin can edit products
* admin can remove products
* admin can upload images for products
* admin can add offers for products
* admin can edit offers for products
* admin can remove offers for products

* client can register
* client can login
* client can reset Lost Password
* client can fetch admin products through api endpoint
* client can see all products
* client can order products per name
* client can filter products per image
* client can search for products at least 2 characters required
* client can go the next page and back to the previous one
* client can't add offers to products unless loggedin
* client can see only his offers that can delete as long as the client is the offer author
* if invalid, none or expired JWTtoken client will be notified (status-401)

## Website

* https://www.youtube.com/watch?v=cIHofbBc1Cg

## Resources

* [Entities properties were created based on, https://developers.google.com/search/docs/data-types/product and Schema.org]
* [https://api-platform.com/docs]
* [LexikJWTAuthenticationBundle documentation, https://github.com/lexik/LexikJWTAuthenticationBundle]

## Built With

* [Symfony]
* [Symfony api_platform]
* [JavaScript]
* [axios library]
* [api_platform]
* []

## Author

* **Zache Abdelatif (Leto)** 